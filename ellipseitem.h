#ifndef ELLIPSEITEM_H
#define ELLIPSEITEM_H

#include <QQuickPaintedItem>
#include <QPainter>


class EllipseItem : public QQuickPaintedItem
{
    Q_OBJECT

    QColor m_color;

    Q_PROPERTY(QColor color READ getColor WRITE setColor NOTIFY colorChanged)

public:
    EllipseItem();
    void paint(QPainter *painter) override;

    QColor getColor();

public slots:
    void setColor(QColor color);

signals:
    void colorChanged(QColor value);
};

#endif // ELLIPSEITEM_H
