#include "led-picture.h"
#include "widget.h"

#include <QDebug>
#include <QLabel>

#include <QVBoxLayout>

#include "ellipseitem.h"

///----------------------------------------------------------------------------
LedPicture::LedPicture(QWidget *parent) :
    QWidget(parent), state(false), color(Qt::white)
{
#if 0
    qmlRegisterType<EllipseItem>("shapes", 1, 0, "Ellipse");

    qml_widget = new QQuickWidget(this) ;
    qml_widget->setSource(QUrl("qrc:/lamp.qml"));
    qml_widget->setResizeMode(QQuickWidget::SizeRootObjectToView);

    auto rootObject = qml_widget->rootObject();
    connect(this, SIGNAL(ColorChanged(QColor)), rootObject, SLOT(setColor(QColor)));

    QVBoxLayout* l = new QVBoxLayout;
    l->addWidget(qml_widget);
    this->setLayout(l);
#endif
}

///----------------------------------------------------------------------------
LedPicture::~LedPicture()
{
}

///----------------------------------------------------------------------------
void LedPicture::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);
#if 0
    painter.setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::FlatCap));
    if (state)
        painter.setBrush(QBrush(color, Qt::SolidPattern));
    else
        painter.setBrush(QBrush(Qt::white, Qt::SolidPattern));

    QPoint center(width()/2,height()/2);
    int rad = qMin(width()/4,height()/4);
    painter.drawEllipse(center, rad, rad);
#endif

    QColor color_;
    QString txt;

    if (state)
    {
        color_ = color;
        txt = "ON";
    }
    else
    {
        color_ = Qt::darkGray;
        color = Qt::white;
        txt = "OFF";
    }

    Lamp l(this->rect(), color_);
    l.setText(txt);
    l.Render(painter);
}

///----------------------------------------------------------------------------
void LedPicture::receive_command(uint8_t cmd)
{
    switch (cmd)
    {
        case ON:
            state = true;
            //emit ColorChanged(Qt::white);
            repaint();
        break;

        case OFF:
            state = false;
            //emit ColorChanged(Qt::black);
            repaint();
        break;
    }
}

///----------------------------------------------------------------------------
void LedPicture::receive_rgb_color(QColor color_)
{
    color = color_;
    emit ColorChanged(color);
    repaint();
}
