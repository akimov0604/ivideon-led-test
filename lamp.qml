import QtQuick 2.0
import shapes 1.0

Rectangle {
    width: 400
    height: 400

    function setColor(value) { myellipse.color = value; update(); }

    Ellipse {
        id: myellipse
        anchors.fill: parent
        color: "red"

        onColorChanged: {
            console.log("onColorChanged")
            color = value
        }
    }
}
