#ifndef LAMP_H
#define LAMP_H

#include <QPainter>
#include <QPainterPath>

class Lamp
{
public:
  Lamp();
  Lamp(QRect rect, QColor color=Qt::red, bool lit=true);
  ~Lamp();

  void Render(QPainter &painter);
  void setText(QString text);

private:
  void Dimension(void);
  QPoint center;

  QRect myRect;
  QPainterPath myPath;
  QLinearGradient myGradient;

  int Width;
//  qreal bezelRadius;

  QRect lampRect;
  QPainterPath lampPath;
  QRadialGradient lampGradient;

  QColor lampColor;
  QColor lampColorLow;
  bool lampLit;
  qreal lampRadius;

  QFont lampFont;
  QString lampText;
};

#endif
