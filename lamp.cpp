#include "lamp.h"

Lamp::Lamp()
{

}

Lamp::Lamp(QRect rect, QColor color, bool lit)
{
  lampColor = color;
  lampLit = lit;
  myRect = rect;

  Dimension();
}

Lamp::~Lamp()
{

}

void Lamp::Dimension()
{
  Width = myRect.height()/12;

  // Lamp bounding rectangle
  lampRect = myRect.adjusted(Width, Width, -Width, -Width);

  // shading
  myGradient = QLinearGradient(myRect.topRight(), myRect.bottomLeft());
  myGradient.setColorAt(0.0, QColor(Qt::darkGray));
  myGradient.setColorAt(1.0, QColor(Qt::black));

  lampRadius = Width * lampRect.width()/myRect.width();

  // Lamp "Bright spot"
  lampGradient = QRadialGradient(lampRect.center(), 2);
  lampGradient.setSpread(QGradient::ReflectSpread);

  lampFont = QFont("Arial", lampRect.height()*0.6, QFont::DemiBold);
}

void Lamp::setText(QString text)
{
  lampText = text;
}

void Lamp::Render(QPainter &painter)
{
  // Set Lamp color intensities
  if (!lampLit) {
    lampColor.setHsv(lampColor.hue(), lampColor.saturation(), lampColor.value()*0.2);
  }
  lampColorLow.setHsv(lampColor.hue(), lampColor.saturation(), lampColor.value()*0.8);

  // Set Lamp gradients
  lampGradient.setColorAt(0.0, lampColor);
  lampGradient.setColorAt(1.0, lampColorLow);

  // Draw everything
  painter.setRenderHint(QPainter::Antialiasing);

  painter.setPen(Qt::NoPen);
  painter.setBrush(myGradient);
  painter.drawRoundedRect(myRect, Width, Width);

  painter.setPen(Qt::NoPen);
  painter.setBrush(lampGradient);
  painter.drawRoundedRect(lampRect, lampRadius, lampRadius);

  painter.setPen(Qt::black);
  painter.setFont(lampFont);
  painter.drawText(lampRect, Qt::AlignCenter ,lampText);

}


