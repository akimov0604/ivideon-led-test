#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QTcpSocket>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QSplitter>

#include <QTextEdit>
#include <QAbstractSocket>

#include "led-picture.h"

class ProtocolLedTest : public QDialog
{
    Q_OBJECT

public:
    ProtocolLedTest(QWidget *parent = 0);
    ~ProtocolLedTest();

private:

    QLabel*           hostLabel;
    QLabel*           portLabel;
    QLabel*           statusLabel;
    QLabel*           image;
    QComboBox*        hostCombo;
    QLineEdit*        portLineEdit;
    QPushButton*      connectButton;
    QPushButton*      disconnectButton;
    QDialogButtonBox* buttonBox;
    QTextEdit*        dumpTextEdit;
    LedPicture*       led_picture;
    QSplitter*        splitter;

    QTcpSocket* socket;



private slots:
    void displayError(QAbstractSocket::SocketError socketError);
    void activateConnectButton();
    void connectToServer();
    void disconnectFromServer();
    void showConnectedStatus();
    void showDisconnectedStatus();
    void readData();

signals:
    void setCommand(uint8_t cmd);
    void setRgb(QColor color);
};

enum protocol_commands
{
    ON = 0x12,
    OFF = 0x13,
    SET_COLOR = 0x20
};

#endif // DIALOG_H
