#include "widget.h"
#include <QGridLayout>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QByteArray>
#include <QPlainTextDocumentLayout>
#include <QVariant>
#include <QColor>
#include <QApplication>
#include <QScreen>
#include <iostream>
#include <arpa/inet.h>

ProtocolLedTest::ProtocolLedTest(QWidget *parent) : QDialog(parent)
{
    QRect r = QApplication::screens().at(0)->geometry();
    ///qDebug() << "width = " << r.width() << " height = " << r.height();
    this->resize(r.width()/2, r.height()/2);

    /* Ньючим виджеты и сокет */
    hostLabel   = new QLabel(tr("&host:"));
    portLabel   = new QLabel(tr("&port:"));
    statusLabel = new QLabel(tr("status:"));

    hostCombo = new QComboBox;
    portLineEdit = new QLineEdit;
    connectButton    = new QPushButton(tr("&Connect"));
    disconnectButton = new QPushButton(tr("&Disconnect"));
    buttonBox = new QDialogButtonBox;
    dumpTextEdit = new QTextEdit;
    splitter = new QSplitter(this);
    led_picture = new LedPicture(this);

    socket = new QTcpSocket(this);

    /* Настраиваем */

    hostCombo->setEditable(true);

    portLineEdit->setValidator(new QIntValidator(1, 65535, this));
    portLineEdit->setFocus();

    hostLabel->setBuddy(hostCombo);
    portLabel->setBuddy(portLineEdit);

    connectButton->setDefault(true);
    connectButton->setEnabled(true);

    buttonBox->addButton(connectButton,    QDialogButtonBox::ActionRole);
    buttonBox->addButton(disconnectButton, QDialogButtonBox::RejectRole);

    QVBoxLayout* mainLayout   = new QVBoxLayout;
    QHBoxLayout* upperLayout  = new QHBoxLayout;
    QGridLayout* bottomLayout = new QGridLayout;

    bottomLayout->addWidget(hostLabel,    0, 0);
    bottomLayout->addWidget(hostCombo,    0, 1);
    bottomLayout->addWidget(portLabel,    1, 0);
    bottomLayout->addWidget(portLineEdit, 1, 1);
    bottomLayout->addWidget(buttonBox,    3, 0, 1, 2);
    bottomLayout->addWidget(statusLabel,  4, 0);

#if 1
    /// отладка
    hostCombo->addItem("emb2.extcam.xyz");
    portLineEdit->setText("9991");
#endif

    splitter->addWidget(dumpTextEdit);
    splitter->addWidget(led_picture);
    upperLayout->addWidget(splitter);

    led_picture->resize(this->width()/4, this->height()/2);

    mainLayout->addLayout(upperLayout);
    mainLayout->addLayout(bottomLayout);

    setLayout(mainLayout);

    setWindowTitle(tr("Ivideon lamp test"));

    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(displayError(QAbstractSocket::SocketError)));
    connect(portLineEdit, SIGNAL(textChanged(QString)), this, SLOT(activateConnectButton()));
    connect(connectButton, SIGNAL(clicked()), this, SLOT(connectToServer()));
    connect(disconnectButton, SIGNAL(clicked()), this, SLOT(disconnectFromServer()));
    connect(socket, SIGNAL(connected()), this, SLOT(showConnectedStatus()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(showDisconnectedStatus()));
    connect(socket, SIGNAL(readyRead()), this, SLOT(readData()));
    connect(this, SIGNAL(setCommand(uint8_t)), led_picture, SLOT(receive_command(uint8_t)));
    connect(this, SIGNAL(setRgb(QColor)), led_picture, SLOT(receive_rgb_color(QColor)));
}

//-----------------------------------------------------------------------------
ProtocolLedTest::~ProtocolLedTest()
{
    if (socket->isOpen()) {
        socket->close();
    }
}

//-----------------------------------------------------------------------------
void ProtocolLedTest::displayError(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(this, tr("Client"),
                                       tr("The host was not found. Please check the "
                                       "host name and port settings."));
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(this, tr("Client"),
                                       tr("The connection was refused by the peer. "
                                       "Make sure the server is running, "
                                       "and check that the host name and port "
                                       "settings are correct."));
        break;
    default:
        QMessageBox::information(this, tr("Client"),
                                       tr("The following error occurred: %1.")
                                       .arg(socket->errorString()));
    }
}

//-----------------------------------------------------------------------------
void ProtocolLedTest::activateConnectButton()
{
    connectButton->setEnabled(!hostCombo->currentText().isEmpty() && !portLineEdit->text().isEmpty());
}

//-----------------------------------------------------------------------------
void ProtocolLedTest::connectToServer()
{
    socket->abort();
    socket->connectToHost(hostCombo->currentText(), portLineEdit->text().toInt());
}

//-----------------------------------------------------------------------------
void ProtocolLedTest::disconnectFromServer()
{
    socket->disconnectFromHost();
}

//-----------------------------------------------------------------------------
void ProtocolLedTest::showConnectedStatus()
{
    statusLabel->setText("status: Connected");
}

//-----------------------------------------------------------------------------
void ProtocolLedTest::showDisconnectedStatus()
{
    statusLabel->setText("status: Disconnected");
}

//----------------------------------------------------------------------------
void ProtocolLedTest::readData()
{
    QByteArray arr = socket->readAll();

    if(arr.length() < 3)
    {
        statusLabel->setText("Error: received less than 3 bytes");
        return;
    }

    QString rx;

    rx.append("RX[");
    rx.append(QVariant(arr.length()).toString());
    rx.append("bytes]:\n");

    QString data;
    data.append(rx);
    data.append(arr.toHex('_'));

    dumpTextEdit->setTextColor(QColor("red"));
    dumpTextEdit->append(data);

    uint8_t* ptr = (uint8_t*)arr.data();

    uint8_t cmd = *ptr;

    switch(cmd)
    {
        case ON:
            {
                emit setCommand(ON);
            }
        break;

        case OFF:
            {
                emit setCommand(OFF);
            }
        break;

        case SET_COLOR:
        {
            ptr++;
            uint16_t len = htons(*(uint16_t*)(ptr));

            if (len != 3)
            {
                statusLabel->setText("Error: undefined rgb len");
                break;
            }

            ptr += 2;
            int r, g, b;
            r = *ptr++;
            g = *ptr++;
            b = *ptr;

           QColor c;
           c.setRgb(r, g, b);

           emit setRgb(c);
        }
        break;

        default:
        {
            statusLabel->setText("Error: uncknown command");
        }
    }
}


