#include "ellipseitem.h"

#include <QGraphicsItem>

///----------------------------------------------------------------------------
EllipseItem::EllipseItem()
{
    setFlag(QQuickPaintedItem::Flag::ItemHasContents, true);
}

///----------------------------------------------------------------------------
void EllipseItem::paint(QPainter *painter)
{
    painter->save();

    //QBrush  brush(Qt::green);
    //painter->setPen(Qt::red);
    //painter->setRenderHints(QPainter::Antialiasing, true);

    painter->setBrush(QBrush(m_color));
    painter->drawEllipse(100, 50, 150, 150);

    painter->restore();
}

QColor EllipseItem::getColor()
{
    return m_color;
}

void EllipseItem::setColor(QColor color)
{
    if (m_color != color)
    {
        m_color = color;
    }
}
