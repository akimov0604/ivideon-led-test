#ifndef LEDPICTURE_H
#define LEDPICTURE_H

#include <QWidget>
#include <QPainter>
#include <QLabel>
#include <QAbstractScrollArea>
#include <QtQuickWidgets/QQuickWidget>
#include "lamp.h"

class LedPicture: public QWidget
{
    Q_OBJECT

public:
    explicit LedPicture(QWidget *parent = 0);
    ~LedPicture();

protected:
    void paintEvent(QPaintEvent *event);

private slots:
    void receive_command(uint8_t cmd);
    void receive_rgb_color(QColor clr);

private:
    bool state;
    QColor color;
    QQuickWidget* qml_widget;


signals:
    void ColorChanged(QColor color);
};

#endif // LEDPICTURE_H
